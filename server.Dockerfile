FROM php:8.0-fpm-alpine

# Create folder to run
RUN mkdir -p /var/www/workmanager-backend

# Work in the specific space
WORKDIR /var/www/workmanager-backend

# Install dependencies
RUN apk add --no-cache \
    freetype \
    libpng \
    libjpeg-turbo \
    freetype-dev \
    libpng-dev \
    libjpeg-turbo-dev

RUN docker-php-ext-configure gd \
    --with-freetype \
    --with-jpeg

RUN NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && \
    docker-php-ext-install -j${NPROC} gd

RUN apk del --no-cache freetype-dev libpng-dev libjpeg-turbo-dev

RUN docker-php-ext-install pdo pdo_mysql \
    sockets
COPY . /var/www/workmanager-backend
RUN chmod -R ugo+rw /var/www/workmanager-backend/storage

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
ENV COMPOSER_ALLOW_SUPERUSER 1
RUN composer install --ignore-platform-reqs --no-dev


