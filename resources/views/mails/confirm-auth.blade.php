<!DOCTYPE html>
<html>
<head>
    <title>Workmanager</title>
</head>
<body>
<p>Здравствуйте, используйте данный код для подтверждения регистрации: <b>{{ $code }}</b></p>
<p>Если это письмо пришло к Вам по ошибке, просто проигнорируйте его.</p>
<br>
<p>Команда Workmanager</p>
</body>
</html>
