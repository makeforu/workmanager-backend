<?php
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'App\Http\Controllers\Api\v1'], function () {

    Route::group(['prefix' => 'welcome'], function () {
        Route::get('','WelcomeController@index');
    });

    Route::group(['prefix' => 'auth'], function () {
        Route::post('signup','AuthController@signup');
        Route::post('confirm', 'AuthController@confirm');
        Route::post('sign-in', 'AuthController@signIn');
        Route::post('forgot-password', 'AuthController@forgotPassword');
        Route::put('reset-password', 'AuthController@resetPassword');
        Route::get('no-login', 'AuthController@noLogin')->name('login');

        Route::group(['middleware' => ['auth:sanctum']], function () {
            Route::post('logout', 'AuthController@logout');
            Route::post('logout-all', 'AuthController@logoutAll');
        });
    });

    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::resource('jobs', 'JobController');
        Route::group(['prefix' => 'profile'], function () {
            Route::get('', 'ProfileController@index');
            Route::put('', 'ProfileController@update');
        });
    });
});
