dev-up: dev-docker-up

dev-build: dev-docker-build-up

dev-build-docs: dev-docker-build-docs

dev-down: dev-docker-down

dev-docker-up:
	@docker-compose -f docker-compose-local.yml up -d

dev-docker-build-up:
	@docker-compose -f docker-compose-local.yml up -d --build

dev-docker-down:
	@docker-compose -f docker-compose-local.yml down

dev-docker-build-docs:
	@docker-compose -f docker-compose-local.yml up -d --no-deps --build docs
