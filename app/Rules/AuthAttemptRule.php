<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class AuthAttemptRule implements Rule
{
    public ?string $email = "";
    public ?string $password = "";

    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return Auth::attempt(['email' => $this->email, 'password' => $value]);
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return "Некорректно указа логин или пароль";
    }
}
