<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use App\Models\User;
use App\Repositories\Eloquent\UserRepository;

class ConfirmCodeRule implements Rule
{
    private ?User $user;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(array $fields)
    {
        $this->user = (new UserRepository())->findBy($fields);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        if ($this->user) return true;
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Неверный код подтверждения.';
    }
}
