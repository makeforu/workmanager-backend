<?php

namespace App\Jobs;

use http\Exception\InvalidArgumentException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Mail\EmailForQueuing;
use Illuminate\Support\Facades\Mail;

/**
 * @deprecated
 */
class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected ?array $details = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $details)
    {
        $this->queue = getenv('QUEUE_NOTIFICATION_EMAIL');
        if (empty($this->queue)) {
            throw new InvalidArgumentException("Set environment QUEUE_NOTIFICATION_EMAIL!");
        }
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new EmailForQueuing($this->details);
        Mail::to($this->details['email'])->send($email);
    }
}
