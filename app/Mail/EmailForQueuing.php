<?php

namespace App\Mail;

use http\Exception\InvalidArgumentException;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;

/**
 * @deprecated
 */
class EmailForQueuing extends Mailable
{
    use Queueable, SerializesModels;

    private string $template;
    public string $code;

    /**
     * Create a new message instance.
     *
     * @param array $details
     */
    public function __construct(array $details)
    {
        foreach ($details as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public function __set($name, $value)
    {
        $this->{$name} = $value;
    }

    public function __get($name)
    {
        return $this->{$name};
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): EmailForQueuing
    {
        return $this->from(Config::get('mail.from.address'), Config::get('mail.from.name'))
                    ->subject($this->subject)
                    ->view($this->template);
    }
}
