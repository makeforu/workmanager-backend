<?php

namespace App\Interfaces\Eloquent;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface BaseRepositoryInterface
{
    /**
     * Get all models.
     *
     * @param array|string[] $columns
     * @param array $relations
     * @return Collection
     */
    public function all(array $columns = ['*'], array $relations = []): Collection;

    /**
     * Find model by id.
     *
     * @param int $model_id
     * @param array|string[] $columns
     * @param array $relations
     * @param array $appends
     * @return Model|null
     */
    public function findById(int $model_id, array $columns = ['*'], array $relations = [], array $appends = []): ?Model;

    /**
     * Find model by array params.
     *
     * @param array $criteria
     * @param array|string[] $columns
     * @param array $relations
     * @param array $appends
     * @return Model|null
     */
    public function findBy(array $criteria, array $columns = ['*'], array $relations = [], array $appends = []): ?Model;

    /**
     * Find query by array params.
     *
     * @param array $criteria
     * @param array|string[] $columns
     * @param array $relations
     * @param array $appends
     * @return mixed
     */
    public function findQueryBy(array $criteria, array $columns = ['*'], array $relations = [], array $appends = []): mixed;

    /**
     * Create a model.
     *
     * @param array $data
     * @return Model|null
     */
    public function create(array $data): ?Model;

    /**
     * Update a model.
     *
     * @param int $model_id
     * @param array $data
     * @return bool
     */
    public function update(int $model_id, array $data): bool;

    /**
     * Delete model by id.
     *
     * @param int $model_id
     * @return bool
     */
    public function deleteById(int $model_id): bool;
}
