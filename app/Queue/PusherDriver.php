<?php
namespace App\Queue;

use http\Exception\InvalidArgumentException;

abstract class PusherDriver
{
    public ?string $queue_driver = null;

    public function __construct()
    {
        $this->init();

        if (empty($this->queue_driver)) {
            throw new InvalidArgumentException('Not set queue driver!');
        }
    }

    abstract protected function init();
}
