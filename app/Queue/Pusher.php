<?php
namespace App\Queue;

class Pusher extends PusherInterface
{
    /**
     * Push message in queue
     *
     * @param string $message
     * @param string $queue_name
     */
    public function push(string $message, string $queue_name)
    {
        $this->getConnection()->pushRaw($message, $queue_name);
    }
}
