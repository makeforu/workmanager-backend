<?php
namespace App\Queue;

abstract class PusherInterface
{
    public mixed $queueManager;
    public PusherDriver $driver;

    public function __construct(string $driver)
    {
        $this->queueManager = app('queue');
        $this->driver = new $driver;
    }

    /**
     * Get queue connection
     *
     * @return mixed
     */
    protected function getConnection(): mixed
    {
        return $this->queueManager->connection($this->driver->queue_driver);
    }

    /**
     * @param string $message
     * @param string $queue_name
     */
    abstract public function push(string $message, string $queue_name);
}
