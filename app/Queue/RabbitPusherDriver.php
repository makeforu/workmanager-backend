<?php
namespace App\Queue;

class RabbitPusherDriver extends PusherDriver
{
    protected function init()
    {
        $this->queue_driver = 'rabbitmq';
    }
}
