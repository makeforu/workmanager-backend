<?php
namespace App\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Model;

use App\Models\User;

/**
 * @method BaseRepository findBy(array $criteria, array $columns = ['*'], array $relations = [], array $appends = [])
 */
class UserRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function getModelClass(): string
    {
        return User::class;
    }
}
