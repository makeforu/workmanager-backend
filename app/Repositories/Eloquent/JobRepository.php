<?php

namespace App\Repositories\Eloquent;

use App\Models\Job as Model;

class JobRepository extends BaseRepository
{
    /**
     * Get class name
     *
     * @return string
     */
    public function getModelClass(): string
    {
        return Model::class;
    }
}
