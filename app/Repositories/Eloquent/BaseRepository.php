<?php

namespace App\Repositories\Eloquent;

use App\Interfaces\Eloquent\BaseRepositoryInterface;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements BaseRepositoryInterface
{
    protected Model $model;

    public function __construct()
    {
        $this->model = new ($this->getModelClass());
    }

    /**
     * @return string
     */
    abstract protected function getModelClass(): string;

    /**
     * @param array|string[] $columns
     * @param array $relations
     * @return Collection
     */
    public function all(array $columns = ['*'], array $relations = []): Collection
    {
        return $this->model->select($columns)->with($relations)->get($columns);
    }

    /**
     * @param int $model_id
     * @param array|string[] $columns
     * @param array $relations
     * @param array $appends
     * @return Model|null
     */
    public function findById(int $model_id, array $columns = ['*'], array $relations = [], array $appends = []): ?Model
    {
        return $this->model->select($columns)->with($relations)->find($model_id)->append($appends);
    }

    /**
     * @param array $criteria
     * @param array|string[] $columns
     * @param array $relations
     * @param array $appends
     * @return mixed
     */
    public function findQueryBy(array $criteria, array $columns = ['*'], array $relations = [], array $appends = []): mixed
    {
        return $this->model->select($columns)->with($relations)->whereRowValues(array_keys($criteria), '=', array_values($criteria));
    }

    /**
     * @param array $criteria
     * @param array|string[] $columns
     * @param array $relations
     * @param array $appends
     * @return Model|null
     */
    public function findBy(array $criteria, array $columns = ['*'], array $relations = [], array $appends = []): ?Model
    {
        return $this->findQueryBy($criteria, $columns, $relations, $appends)->first();
    }

    /**
     * @param array $data
     * @return Model|null
     */
    public function create(array $data): ?Model
    {
       $model = $this->model->create($data);
       return $model->fresh();
    }

    /**
     * @param int $model_id
     * @param array $data
     * @return bool
     */
    public function update(int $model_id, array $data): bool
    {
        return $this->findById($model_id)->update($data);
    }

    /**
     * @param int $model_id
     * @return bool
     */
    public function deleteById(int $model_id): bool
    {
        return $this->findById($model_id)->delete();
    }
}
