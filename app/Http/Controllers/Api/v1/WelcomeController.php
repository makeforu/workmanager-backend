<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\{Request, JsonResponse};

class WelcomeController extends Controller
{
    /**
     * Welcome
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return response()->json([
            'data' => [
                'message' => 'Welcome to API version 1'
            ]
        ]);
    }
}
