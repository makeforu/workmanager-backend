<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Services\AuthService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

use App\Http\Resources\JobResource;
use App\Traits\ApiResponse;
use App\Repositories\Eloquent\JobRepository;
use App\Http\Requests\JobPostRequest;
use App\Models\Job;

class JobController extends Controller
{
    use ApiResponse;

    public function __construct(
        private JobRepository $jobRepository,
        private AuthService   $authService
    ) {
        parent::__constructor();
    }

    /**
     * Display a listing of the resource work.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $user = $this->authService->getUser();
        $jobs = $this->jobRepository->findQueryBy(['user_id' => $user->id])->get();
        return $this->success(data: JobResource::collection($jobs));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param JobPostRequest $request
     * @return JsonResponse
     */
    public function store(JobPostRequest $request): JsonResponse
    {
        $job = $this->jobRepository->create($request->validated());
        return $this->success(ResponseAlias::HTTP_CREATED, new JobResource($job));
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function show(Request $request, int $id): JsonResponse
    {
        $user = $this->authService->getUser();
        if ($job = $this->jobRepository->findBy(['id' => $id, 'user_id' => $user->id])) {
            return $this->success(data: new JobResource($job));
        }
        return $this->error(ResponseAlias::HTTP_NOT_FOUND, "Job #$id not found.");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param JobPostRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(JobPostRequest $request, int $id): JsonResponse
    {
        /**
         * @var Job $job
         */
        $user = $this->authService->getUser();
        if ($job = $this->jobRepository->findBy(['id' => $id, 'user_id' => $user->id])) {
            $this->jobRepository->update($job->id, $request->validated());
            return $this->success(data: new JobResource($job->refresh()));
        }
        return $this->error(ResponseAlias::HTTP_NOT_FOUND, "Job #$id not found.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(Request $request, int $id): JsonResponse
    {
        /**
         * @var Job $job
         */
        $user = $this->authService->getUser();
        if ($job = $this->jobRepository->findBy(['id' => $id, 'user_id' => $user->id])) {
            $this->jobRepository->deleteById($job->id);
            return $this->success(ResponseAlias::HTTP_NO_CONTENT);
        }
        return $this->error(ResponseAlias::HTTP_NOT_FOUND, "Job #$id not found.");
    }
}
