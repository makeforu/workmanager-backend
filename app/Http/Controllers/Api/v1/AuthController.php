<?php

namespace App\Http\Controllers\Api\v1;

use App\Repositories\Eloquent\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\{
    SignupPostRequest,
    SignInPostRequest,
    ForgotPasswordPostRequest,
    ResetPasswordPutRequest,
    ConfirmPostRequest
};
use App\Services\AuthService;
use App\Services\Notification\EmailService;
use App\Traits\ApiResponse;
use App\Http\Resources\UserResource;

class AuthController extends Controller
{
    use ApiResponse;

    public function __construct(
        private UserRepository $userRepository,
        private EmailService $emailService,
        private AuthService $authService
    ) {
        parent::__constructor();
    }

    /**
     * Sign up user
     *
     * @param SignupPostRequest $request
     * @return JsonResponse
     */
    public function signup(SignupPostRequest $request): JsonResponse
    {
        $data = array_merge(
            $request->validated(),
            ['confirm_code' => $this->emailService->sendConfirmCode($request->json('email'))]
        );
        $user = $this->userRepository->create($data);
        return $this->success( ResponseAlias::HTTP_CREATED, new UserResource($user));
    }

    /**
     * Confirm sign-up
     *
     * @param ConfirmPostRequest $request
     * @return JsonResponse
     */
    public function confirm(ConfirmPostRequest $request): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = $this->userRepository->findBy($request->only(['email', 'confirm_code']));
        $this->userRepository->update(
            $user->id,
            ['status' => User::STATUS_ACTIVE, 'confirm_code' => null]
        );
        return $this->success(
            ResponseAlias::HTTP_OK,
            ['token' => $user->createToken('Bearer')->plainTextToken]
        );
    }

    /**
     * Sign in user
     *
     * @param SignInPostRequest $request
     * @return JsonResponse
     */
    public function signIn(SignInPostRequest $request): JsonResponse
    {
        return $this->success(
            ResponseAlias::HTTP_OK,
            ['token' => auth()->user()->createToken('Bearer')->plainTextToken]
        );
    }

    /**
     * Forgot password
     *
     * @param ForgotPasswordPostRequest $request
     * @return JsonResponse
     */
    public function forgotPassword(ForgotPasswordPostRequest $request): JsonResponse
    {
        /**
         * @var User $user
         */
        if ($user = $this->userRepository->findBy($request->only(['email']))) {
            $this->userRepository->update(
                $user->id,
                ['password_reset_code' => $this->emailService->sendPasswordResetCode($user->email)]
            );
            return $this->success(ResponseAlias::HTTP_NO_CONTENT);
        }
        return $this->error(ResponseAlias::HTTP_NOT_FOUND, 'User not found.');
    }

    /**
     * Reset password
     *
     * @param ResetPasswordPutRequest $request
     * @return JsonResponse
     */
    public function resetPassword(ResetPasswordPutRequest $request): JsonResponse
    {
        /**
         * @var User $user
         */
        if ($user = $this->userRepository->findBy($request->only(['email', 'password_reset_code']))) {
            $this->authService->resetPassword($user, $request->json('password'));
            return $this->success(ResponseAlias::HTTP_NO_CONTENT);
        }
        return $this->error(ResponseAlias::HTTP_NOT_FOUND, 'User not found.');
    }

    /**
     * Logout
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $user = $this->authService->getUser();
        $user->currentAccessToken()->delete();
        return $this->success(ResponseAlias::HTTP_NO_CONTENT);
    }

    /**
     * Logout all
     * @param Request $request
     * @return JsonResponse
     */
    public function logoutAll(Request $request): JsonResponse
    {
        $user = $this->authService->getUser();
        $user->tokens()->delete();
        return $this->success(ResponseAlias::HTTP_NO_CONTENT);
    }

    /**
     * No login (fake action)
     *
     * @return JsonResponse
     */
    public function noLogin(): JsonResponse
    {
        return $this->error(ResponseAlias::HTTP_UNAUTHORIZED, 'Your request was made with invalid credentials.');
    }
}
