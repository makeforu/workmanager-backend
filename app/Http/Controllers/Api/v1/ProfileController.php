<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

use App\Traits\ApiResponse;
use App\Repositories\Eloquent\UserRepository;
use App\Http\Resources\UserResource;
use App\Services\AuthService;
use App\Http\Requests\ProfileUpdatePutRequest;

class ProfileController extends Controller
{
    use ApiResponse;

    public function __construct(
        private UserRepository $userRepository,
        private AuthService $authService
    ) {
        parent::__constructor();
    }

    /**
     * Display user profile.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $user = $this->authService->getUser();
        return $this->success(data: new UserResource($user));
    }

    /**
     * Update user profile.
     *
     * @param ProfileUpdatePutRequest $request
     * @return JsonResponse
     */
    public function update(ProfileUpdatePutRequest $request): JsonResponse
    {
        $user = $this->authService->getUser();
        $this->userRepository->update($user->id, $request->validated());

        return $this->success(data: new UserResource($user->refresh()));
    }
}
