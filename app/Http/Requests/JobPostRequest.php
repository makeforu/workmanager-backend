<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Services\AuthService;
use JetBrains\PhpStorm\ArrayShape;

class JobPostRequest extends FormRequest
{
    public function __construct(
        public AuthService $authService,
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    ) {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['user_id' => "string[]", 'name' => "string[]", 'description' => "string[]", 'hourly_pay' => "string[]", 'tax_rate' => "string[]"])]
    public function rules(): array
    {
        return [
            'user_id' => ['required', 'integer', 'exists:users,id'],
            'name' => ['required', 'max:100'],
            'description' => ['max:255'],
            'hourly_pay' => ['numeric', 'regex:/^-?[0-9]+(?:.[0-9]{1,2})?$/'],
            'tax_rate' => ['numeric', 'regex:/^-?[0-9]+(?:.[0-9]{1,2})?$/']
        ];
    }

    public function prepareForValidation()
    {
        $user = $this->authService->getUser();
        $this->merge(['user_id' => $user->id]);
    }
}
