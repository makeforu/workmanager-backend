<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

class ProfileUpdatePutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['firstname' => "string[]", 'lastname' => "string[]"])]
    public function rules(): array
    {
        return [
            'firstname' => ['max:100'],
            'lastname' => ['max:100']
        ];
    }
}
