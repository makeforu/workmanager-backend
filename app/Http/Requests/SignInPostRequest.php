<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\AuthAttemptRule;

class SignInPostRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'max:100', 'email', 'exists:users'],
            'password' => ['required', 'string', new AuthAttemptRule($this->json('email'), $this->json('password'))]
        ];
    }
}
