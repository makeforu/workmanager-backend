<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ResetCodeRule;
use JetBrains\PhpStorm\ArrayShape;

class ResetPasswordPutRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['email' => "string[]", 'password_reset_code' => "array", 'password' => "string[]"])]
    public function rules(): array
    {
        return [
            'email' => ['required', 'max:100', 'email'],
            'password_reset_code' => ['required', 'max:6', 'min:6', new ResetCodeRule($this->only(['email', 'password_reset_code']))],
            'password' => ['required', 'string', 'min:6', 'confirmed']
        ];
    }
}
