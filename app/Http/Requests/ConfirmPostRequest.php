<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\ConfirmCodeRule;

class ConfirmPostRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'max:100', 'email'],
            'confirm_code' => ['required', 'max:6', 'min:6', new ConfirmCodeRule($this->only(['email', 'confirm_code']))]
        ];
    }
}
