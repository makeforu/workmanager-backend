<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Interfaces\Eloquent\{
    BaseRepositoryInterface
};
use App\Repositories\Eloquent\{
    BaseRepository,
    UserRepository,
    JobRepository
};

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BaseRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(UserRepository::class);
        $this->app->bind(JobRepository::class);
    }
}
