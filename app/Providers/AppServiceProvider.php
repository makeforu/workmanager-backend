<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Queue\Pusher;
use App\Queue\RabbitPusherDriver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Pusher::class, function($app) {
            return new Pusher(RabbitPusherDriver::class);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
