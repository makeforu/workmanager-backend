<?php

namespace App\Services;

use App\Repositories\Eloquent\UserRepository;
use App\Models\User;

class AuthService
{
    public function __construct(
        private UserRepository $userRepository
    ) { }

    /**
     * Reset user password
     *
     * @param User $user
     * @param string $password
     */
    public function resetPassword(User $user, string $password): void
    {
        $this->userRepository->update(
            $user->id,
            ['password_reset_code' => null, 'password' => $password]
        );
        $this->removeAccessTokens($user);
    }

    /**
     * Remove all access tokens
     *
     * @param User $user
     * @return mixed
     */
    public function removeAccessTokens(User $user): mixed
    {
        return $user->tokens()->delete();
    }

    /**
     * Get auth user
     *
     * @return User
     */
    public function getUser(): User
    {
        return auth()->user();
    }
}
