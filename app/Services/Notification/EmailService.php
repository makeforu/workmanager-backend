<?php

namespace App\Services\Notification;

use App\Jobs\SendEmail;
use App\Queue\Pusher;

class EmailService
{
    public function __construct(
        private Pusher $pusher
    ) {

    }

    /**
     * Send email reset code
     *
     * @param string $email
     * @return string
     */
    public function sendPasswordResetCode(string $email): string
    {
        $code = $this->getCode();
        $data = [
            'email' => $email,
            'template' => 'forgot-password',
            'subject' => 'Восстановление пароля',
            'params' => [
                'code' => $code
            ]
        ];
        $this->pusher->push(json_encode($data), getenv('QUEUE_NOTIFICATION_EMAIL'));
        return $code;
    }

    /**
     * Send email confirm code
     *
     * @param string $email
     * @return string
     */
    public function sendConfirmCode(string $email): string
    {
        $code = $this->getCode();
        $data = [
            'template' => 'confirm-auth',
            'email' => $email,
            'subject' => 'Регистрация Workmanager',
            'params' => [
                'code' => $code
            ]
        ];
        $this->pusher->push(json_encode($data), getenv('QUEUE_NOTIFICATION_EMAIL'));
        return $code;
    }

    /**
     * Get code
     *
     * @return int
     */
    private function getCode(): int
    {
        return rand(111111, 999999);
    }
}
