<?php

namespace App\Traits;

use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

trait ApiResponse
{
    /**
     * Return a success JSON response.
     *
     * @param int $code
     * @param string|null $message
     * @param array|null $data
     * @return JsonResponse
     */
    public function success(int $code = ResponseAlias::HTTP_OK, array|JsonResource $data = null, ?string $message = null): JsonResponse
    {
        if ($code === ResponseAlias::HTTP_NO_CONTENT) {
            return response()->json(null, ResponseAlias::HTTP_NO_CONTENT);
        }
        return response()->json([
            'message' => $message,
            'data' => $data
        ], $code);
    }

    /**
     * Return an error JSON response.
     *
     * @param int $code
     * @param string|null $message
     * @param array|null $data
     * @return JsonResponse
     */
    protected function error(int $code = ResponseAlias::HTTP_NOT_FOUND, string $message = null, ?array $data = null): JsonResponse
    {
        return response()->json([
            'message' => $message,
            'data' => $data
        ], $code);
    }
}
